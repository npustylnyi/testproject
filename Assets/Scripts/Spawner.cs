using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject pipePrefab;

    public float spawnRate = 1.0f;

    public float minHight = -1.0f;
    public float maxHight = 1.0f;

    private void Awake()
    {
        InvokeRepeating("Spawn", spawnRate, spawnRate);
    }

    private void OnDisable()
    {
        CancelInvoke("Spawn");
    }

    private void Spawn()
    {
        GameObject pipes = Instantiate(pipePrefab, transform.position, Quaternion.identity);
        pipes.transform.position += Vector3.up * Random.Range(minHight, maxHight); 
    }
}
